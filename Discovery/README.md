# Discovery Tool

Re-write of the discovery.sh script in Go.

## Required External Tools

These are external tools required for all functionality. The binaries should be installed and available in your $PATH.

- nmap: https://nmap.org/
- amass: https://github.com/OWASP/Amass
- aquatone: https://github.com/michenriksen/aquatone
- gobuster: https://github.com/OJ/gobuster

## Usage

This is the basic usage for now. This is very much in alpha stage, so is subject to change.

`discovery [nmap|rdns|dnsrecon|ctfr|amass|aquatone|gobuster|all] HostFile OutputDirectory [Wordlist]`