module Discovery

go 1.16

require (
	github.com/Ullaakut/nmap/v2 v2.1.1
	github.com/miekg/dns v1.1.42
	github.com/mpvl/unique v0.0.0-20150818121801-cbe035fff7de
)
