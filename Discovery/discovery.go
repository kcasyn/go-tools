package main

import (
	"fmt"
	"net"
	"net/http"
	"net/url"
	"strings"
	"bytes"
	"os"
	"os/exec"
	"io"
	"io/ioutil"
	"regexp"
	"encoding/binary"
	"encoding/json"
	"encoding/csv"
	"path/filepath"
	//crypto/tls
	// github packages
	"github.com/mpvl/unique"
	"github.com/miekg/dns"
	"github.com/Ullaakut/nmap/v2"
	"golang.org/x/net/publicsuffix"
)

/*
References for external packages
nmap:https://github.com/Ullaakut/nmap
ctfr:https://pkg.go.dev/github.com/netevert/delator
gobuster:https://pkg.go.dev/github.com/oj/gobuster

References for external tools
aquatone:https://github.com/michenriksen/aquatone
amass:https://github.com/OWASP/Amass
*/

// TODO

/*
Tasks:
- optimization
- concurrency
Tool Implementation:

Functionality:
- DNSSEC check
- SSL Cert scanning
- Parse out discovered IP/domains/sub-domains
- ADS Discovery (NTLM,Lync,Autodiscover,SSL Certs,Azure)
- everything writes to files

// Completed

Tool Implementation
- amass
- aquatone
- ctfr
- gobuster
- nmap
Functionality
- DNS Recon
- nmap scans (ping,TCP,UDP)
- RDNS
- SMTP checks: Identify presence/absence of SPF/DMARC/DKIM
- Sub domain brute force (gobuster)
- web scraping (aquatone)
*/

//----------HELPERS----------

// funtion to check and print errors
func check(e error) {
	if e != nil {
		fmt.Println(e)
	}
}

// function to check and print warnings
func warn(w []string) {
	if w != nil {
		fmt.Println(w)
	}
}

//----------TYPES----------

// data struct for ctfr
type data struct {
	IssuerCaID        int    `json:"issuer_ca_id"`
	IssuerName        string `json:"issuer_name"`
	NameValue         string `json:"name_value"`
	MinCertID         int    `json:"min_cert_id"`
	MinEntryTimestamp string `json:"min_entry_timestamp"`
	NotAfter          string `json:"not_after"`
	NotBefore         string `json:"not_before"`
}

//----------TOOLS----------

// perform nmap scans
func nmapScan(ips []string, outDir string) {
	fmt.Println("[+] Performing nmap scans")

	// make nmap directory for results
	nmapOutDir := filepath.Join(outDir, "nmap")
	os.Mkdir(nmapOutDir, 0777)

	// nmap -sn <targets> -oG -oN
	pingScan, err := nmap.NewScanner(
		nmap.WithPingScan(),
		nmap.WithTargets(ips[:]...),
		nmap.WithGrepOutput(filepath.Join(nmapOutDir, "ping-scan.gnmap")),
		nmap.WithNmapOutput(filepath.Join(nmapOutDir, "ping-scan.nmap")),
	)
	check(err)

	fmt.Println("[+] Performing nmap ping scan")
	resultPing, warnings, err := pingScan.Run()
	check(err)
	warn(warnings)

	// store alive hosts
	var alive []string
	for _, host := range resultPing.Hosts {
		if host.Status.State == "up" {
			alive = append(alive, host.Addresses[0].Addr)
		}
	}

	fmt.Println("[+] Completed nmap ping scan")
	
	// nmap -sT -p <ports> <targets> -oG -oN
	tcpScan, err := nmap.NewScanner(
		nmap.WithTargets(ips[:]...),
		nmap.WithPorts("21,22,23,25,53,79,80,81,110,139,143,443,445,465,514,993,1433,3306,1521,5432,2902,5800,5900,3389,8000,8300,8080,8500,8501,8433,8888,51010,9090,9100,10000"),
		nmap.WithGrepOutput(filepath.Join(nmapOutDir, "tcp-disco.gnmap")),
		nmap.WithNmapOutput(filepath.Join(nmapOutDir, "tcp-disco.nmap")),
	)
	check(err)

	fmt.Println("[+] Performing nmap TCP discovery scan")
	resultTCP, warnings, err := tcpScan.Run()
	check(err)
	warn(warnings)

	// add to alive hosts
	for _, host := range resultTCP.Hosts {
		if len(host.Ports) != 0 || len(host.Addresses) != 0 {
			alive = append(alive, host.Addresses[0].Addr)
		}
	}

	fmt.Println("[+] Completed nmap TCP discovery scan")

	// sort and unique alive hosts
	unique.Sort(unique.StringSlice{&alive})
	unique.Strings(&alive)

	// write alive hosts to file
	f, err := os.Create(filepath.Join(outDir, "alive.txt"))
	check(err)
	defer f.Close()

	for _, ip := range alive {
		_, err := f.WriteString(ip + "\n")
		check(err)
	}

	f.Sync()

	// nmap -sT -p- <targets> -oG -oN
	tcpScanFull, err := nmap.NewScanner(
		nmap.WithTargets(alive[:]...),
		nmap.WithPorts("1-65535"),
		nmap.WithGrepOutput(filepath.Join(nmapOutDir, "tcp-full.gnmap")),
		nmap.WithNmapOutput(filepath.Join(nmapOutDir, "tcp-full.nmap")),
	)
	check(err)

	// Print the alive hosts
	fmt.Println("Alive hosts:")
	for _, ip := range alive {
		fmt.Println(ip)
	}

	fmt.Println("[+] Performing nmap full TCP scan")
	resultTCPFull, warnings, err := tcpScanFull.Run()
	check(err)
	warn(warnings)

	// print the open ports for each host
	for _, host := range resultTCPFull.Hosts {
		if len(host.Ports) == 0 || len(host.Addresses) == 0 {
			continue
		}

		fmt.Printf("Host %q:\n", host.Addresses[0])

		for _, port := range host.Ports {
			fmt.Printf("\tPort %d/%s %s %s\n", port.ID, port.Protocol, port.State, port.Service.Name)
		}
	}

	fmt.Println("[+] Completed nmap full TCP scan")

	// nmap -sU -p <ports> <targets> -oG -oN
	udpScan, err := nmap.NewScanner(
		nmap.WithTargets(ips[:]...),
		nmap.WithPorts("53,69,161,111,123,514"),
		nmap.WithUDPScan(),
		nmap.WithGrepOutput(filepath.Join(nmapOutDir, "udp-disco.gnmap")),
		nmap.WithNmapOutput(filepath.Join(nmapOutDir, "udp-disco.nmap")),
	)
	check(err)

	fmt.Println("[+] Performing nmap UDP discovery scan")
	resultUDP, warnings, err := udpScan.Run()
	check(err)
	warn(warnings)

	// print the open ports for each host
	for _, host := range resultUDP.Hosts {
		if len(host.Ports) == 0 || len(host.Addresses) == 0 {
			continue
		}

		fmt.Printf("Host %q:\n", host.Addresses[0])

		for _, port := range host.Ports {
			fmt.Printf("\tPort %d/%s %s %s\n", port.ID, port.Protocol, port.State, port.Service.Name)
		}
	}

	fmt.Println("[+] Completed nmap UDP discovery scan")
	fmt.Println("[+] Completed nmap scans")
}

// perform a reverse DNS lookup on provided IPs
func rDNS(ips []string, outDir string) {
	fmt.Println("[+] Performing RDNS lookups")

	// create and open file to write data to
	f, err := os.Create(filepath.Join(outDir, "rdns.txt"))
	check(err)
	defer f.Close()

	// perform a lookup on each ip
	for _, ip := range ips {
		domains, err := net.LookupAddr(ip)
		
		if err == nil && len(domains) > 0 {
			for _, domain := range domains {
				txt := ip + ":" + domain
				fmt.Println(txt)
				_, err := f.WriteString(txt + "\n")
				check(err)
			}
		} else {
			fmt.Println(ip + ":No records")
		}
	}
	f.Sync()

	fmt.Println("[+] Completed RDNS lookups")
}

// perform various DNS record lookups and checks
func dnsRecon(domains []string, outDir string) {
	fmt.Println("[+] Performing DNS recon")

	dnsOutDir := filepath.Join(outDir, "dns")
	os.Mkdir(dnsOutDir, 0777)

	for _, domain := range domains {
		fmt.Println("[+] Performing DNS recon on ", domain)
		// create and open file to write data to
		f, err := os.Create(filepath.Join(dnsOutDir, domain + ".txt"))
		check(err)
		defer f.Close()

		// check TXT records
		txts, err := net.LookupTXT(domain)
		if err == nil && len(txts) > 0 {
			for _, txt := range txts {
				if strings.Contains(txt, "spf") {
		       		fmt.Println("SPF:" + txt)
		       		_, err := f.WriteString(txt + "\n")
					check(err)
		    		} else {
					fmt.Println("TXT:" + txt)
					_, err := f.WriteString(txt + "\n")
					check(err)
				}
			}
		} else {
			fmt.Println("TXT:No records")
		}

		// check DMARC records
		dmarcs, err := net.LookupTXT("_dmarc." + domain)
		if err == nil && len(dmarcs) > 0 {
			for _, dmarc := range dmarcs {
				fmt.Println("DMARC:" + dmarc)
				_, err := f.WriteString(dmarc + "\n")
				check(err)
			}
		} else {
			fmt.Println("DMARC:No records")
		}

		// check CNAME records
		cname, err := net.LookupCNAME(domain)
		if err == nil && len(cname) > 0 {
			fmt.Println("CNAME:" + cname)
			_, err := f.WriteString(cname + "\n")
			check(err)
		} else {
			fmt.Println("CNAME:No records")
		}

		// check MX records
		mxs, err := net.LookupMX(domain)
		if err == nil && len(mxs) > 0 {
			for _, mx := range mxs {
				fmt.Println("MX:" + mx.Host)
				_, err := f.WriteString(mx.Host + "\n")
				check(err)
			}
		} else {
			fmt.Println("MX:No records")
		}

		// check NS records
		nss, err := net.LookupNS(domain)
		if err == nil && len(nss) > 0 {
			for _, ns := range nss {
				fmt.Println("NS:" + ns.Host)
				_, err := f.WriteString(ns.Host + "\n")
				check(err)
			}
		} else {
			fmt.Println("NS:No records")
		}

		// perform an AXFR request
		if err == nil && len(nss) > 0 {
			for _, s := range nss {
				tr := dns.Transfer{}
				m := new(dns.Msg)
				m.SetAxfr(dns.Fqdn(domain))
				in, err := tr.In(m, s.Host+":53")
				if err == nil && len(in) != 0 {
					for ex := range in {
						for _, a := range ex.RR {
							var record, ip, hostname string
							switch v := a.(type) {
							case *dns.A:
								record = "A:"
								ip = v.A.String()
								hostname = v.Hdr.Name
							case *dns.AAAA:
								record = "AAAA:"
								ip = v.AAAA.String()
								hostname = v.Hdr.Name
							case *dns.PTR:
								record = "PTR:"
								ip = v.Hdr.Name
								hostname = v.Ptr
							case *dns.NS:
								cip, err := net.LookupAddr(v.Ns)
								if err != nil || len(cip) == 0 {
									continue
								}
								record = "NS:"
								ip = cip[0]
								hostname = v.Ns
							case *dns.CNAME:
								cip, err := net.LookupAddr(v.Target)
								if err != nil || len(cip) == 0 {
									continue
								}
								record = "CNAME:"
								ip = cip[0]
								hostname = v.Hdr.Name
							case *dns.SRV:
								cip, err := net.LookupAddr(v.Target)
								if err != nil || len(cip) == 0 {
									continue
								}
								record = "SRV:"
								ip = cip[0]
								hostname = v.Target
							default:
								continue
							}
							txt := record + ip + strings.TrimRight(hostname, ".")
							fmt.Println(txt)
							_, err := f.WriteString(txt + "\n")
							check(err)
						}
					}
				} else {
					fmt.Println("DNS Zone Transfer failed")
				}
			}
		} else {
			fmt.Println("DNS Zone Transfer failed")
		}
		f.Sync()
	}

	fmt.Println("[+] Completed DNS recon")
}

func ctfr (domains []string, outDir string) {
	fmt.Println("[+] Performing CTFR lookups")

	// initialize variables
	var keys []data

	// retrieve results from crt.sh
	for _, domain := range domains {
		pubSuffixDomain, _ := publicsuffix.EffectiveTLDPlusOne(domain)
		checkDomain := "%." + pubSuffixDomain
		resp, err := http.Get(fmt.Sprintf("https://crt.sh/?q=%s&output=json", checkDomain))
		if err, ok := err.(*url.Error); ok {
			if err.Timeout() {
				fmt.Println("request timed out")
			} else if err.Temporary() {
				fmt.Println("temporary error")
			} else {
				fmt.Println(fmt.Sprintf("%s", err.Err))
			}
		}
		if resp.StatusCode != 200 {
			fmt.Println(fmt.Sprintf("unexpected status code returned: %d", resp.StatusCode))
		}
		body, err := ioutil.ReadAll(resp.Body)
		check(err)

		json.Unmarshal([]byte(body), &keys)
	}

	// write results if any to csv
	counter := make(map[string]int)
	file, err := os.Create(filepath.Join(outDir, "ctfr.csv"))
	check(err)
	defer file.Close()

	writer := csv.NewWriter(file)
	defer writer.Flush()

	for _, i := range keys {
		counter[i.NameValue]++
		if counter[i.NameValue] == 1 {
			var tmp = []string{i.NameValue}
			err := writer.Write(tmp)
			check(err)
		}
	}

	fmt.Println("[+] Completed CTFR lookups")
}

func ads (ips []string, domains []string, outDir string) {
	fmt.Println("[+] Performing ADS scans")

	adsOutDir := filepath.Join(outDir, "ads")
	os.Mkdir(adsOutDir, 0777)

	for _, domain := range domains {
		tr := &http.Transport{
			TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
		}
		client := &http.Client{Transport: tr}

		req, err := http.NewRequest("HEAD", "https://lyncdiscover." + domain, nil)
		check(err)

		resp, err := client.Do(req)
		check(err)
		defer resp.Body.Close()

		file, err := os.Create(filepath.Join(adsOutDir, domain + ".lyncdiscover.txt"))
		check(err)
	}

	for _, domain := range domains {
		tr := &http.Transport{
			TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
		}
		client := &http.Client{Transport: tr}

		req, err := http.NewRequest("HEAD", "https://autodiscover." + domain, nil)
		check(err)
		req.SetBasicAuth("user", "password")

		resp, err := client.Do(req)
		check(err)
		defer resp.Body.Close()

		file, err := os.Create(filepath.Join(adsOutDir, domain + ".autodiscover.txt"))
		check(err)
	}

	nmapNTLM, err := nmap.NewScanner(
		nmap.WithTargets(ips[:]...),
		nmap.WithPorts("80,443"),
		nmap.WithScripts("http-ntlm-info")
		nmap.WithGrepOutput(filepath.Join(adsOutDir, "ads-ntlm-http.gnmap")),
		nmap.WithNmapOutput(filepath.Join(adsOutDir, "ads-ntlm-http.nmap")),
	)
	check(err)

	_, warnings, err := nmapNTLM.Run()
	check(err)
	warn(warnings)
	
	fmt.Println("[+] Completed ADS scans")
}

//----------WRAPPERS----------

// wrappers for tools that are too difficult to re-implement (at least until I figure out an easy way to add them)
// these are standalone go binaries so they should be able to be installed on either linux or windows, just add the binary to your PATH

func amass (domains []string, outDir string) {
	fmt.Println("[+] Performing amass scans")

	// create amass output directory
	amassOutDir := filepath.Join(outDir, "amass")
	os.Mkdir(amassOutDir, 0777)
	// perform both a passive enum and intel whois scan with amass on each domain
	for _, domain := range domains {
		passiveCmd := exec.Command("amass", "enum", "-passive", "-d", domain, "-o", filepath.Join(amassOutDir, domain + ".passive.txt"))
		intelCmd := exec.Command("amass", "intel", "-whois", "-d", domain, "-o", filepath.Join(amassOutDir, domain + ".whois.txt"))
		err1 := passiveCmd.Run()
		check(err1)
		err2 := intelCmd.Run()
		check(err2)
	}

	fmt.Println("[+] Completed amass scans")
}

func aquatone (ips []string, domains []string, outDir string) {
	fmt.Println("[+] Performing aquatone web scraping")

	// create lists of ips and domains seperated by spaces
	ipList := strings.Join(ips, " ")
	domainList := strings.Join(domains, " ")
	aquatoneOutDir := filepath.Join(outDir, "aquatone")
	// create the echo and aquatone commands
	cmdEcho := exec.Command("echo", ipList, domainList)
	cmdAquatone := exec.Command("aquatone", "-ports", "xlarge", "-out", aquatoneOutDir)
	// create read and write pipes
	r, w := io.Pipe()
	cmdEcho.Stdout = w
	cmdAquatone.Stdin = r
	// create a buffer to hold output
	var b bytes.Buffer
	cmdAquatone.Stdout = &b
	// start each command
	cmdEcho.Start()
	cmdAquatone.Start()
	// wait echo command to write to w
	cmdEcho.Wait()
	// close the writer to pass the output to aquatone
	w.Close()
	// wait on aquatone command to write output to buffer
	cmdAquatone.Wait()
	// print aquatone output to stdout
	io.Copy(os.Stdout, &b)

	fmt.Println("[+] Completed aquatone web scraping")
}

func gobuster (domains []string, outDir string, wordlist string) {
	fmt.Println("[+] Performing subdomain brute forcing with gobuster")

	gobusterOutDir := filepath.Join(outDir, "gobuster")
	os.Mkdir(subdomainOutDir, 0777)

	for _, domain := range domains {
		cmd := exec.Command("gobuster", "-t", "50", "-d", domain, "-w", wordlist, "-o", gobusterOutDir)
		err := cmd.Run()
		check(err)
	}

	fmt.Println("[+] Completed subdomain bruteforcing")
}

//----------PARSING----------

// Convert CIDRs to an IP list
func convertCIDR (netw string) []string {
	// convert string to IPNet struct
	_, ipv4Net, _ := net.ParseCIDR(netw)

	// convert IPNet struct mask and address to uint32
	mask := binary.BigEndian.Uint32(ipv4Net.Mask)
	// find the start IP address
	start := binary.BigEndian.Uint32(ipv4Net.IP)
	// find the final IP address
	finish := (start & mask) | (mask ^ 0xffffffff)
	// make a slice to return host addresses
	var hosts []string
	// loop through addresses as uint32.
	for i := start; i <= finish; i++ {
		// convert back to net.IPs
        	// Create IP address of type net.IP. IPv4 is 4 bytes, IPv6 is 16 bytes.
		ip := make(net.IP, 4)
		binary.BigEndian.PutUint32(ip, i)
		hosts = append(hosts, ip.String())
	}
	// return a slice of strings containing IP addresses
	return hosts
}

// Create IP list scope
func createIpList (hostFileText string) []string {
	// Regex for IPs
	ipRegex, _ := regexp.Compile(`(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)(\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)){3}$`)
	// Regex for CIDRs
	cidrRegex, _ := regexp.Compile(`(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)(\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)){3}/[1-3]{0,1}[0-9]`)
	// Parse our CIDR ranges, expand to single IPs, and add to IP list
	cidrs := cidrRegex.FindAllString(hostFileText, -1)
	var ips []string
	for _, cidr := range cidrs {
		cidrList := convertCIDR(cidr)
		for _, host := range cidrList {
			ips = append(ips, host)
		}		
	}
	// Add any other IPs to the IP list
	tempips := ipRegex.FindAllString(hostFileText, -1)
	for _, tempip := range tempips {
		ips = append(ips, tempip)
	}
	// Sort and unique the IP list
	unique.Sort(unique.StringSlice{&ips})
	unique.Strings(&ips)
	// Return the IP list
	return ips
}

// Create Domain list for scope
func createDomainList (hostFileText string) []string {
	// Regex for domains
	domainRegex, _ := regexp.Compile(`.*\.([a-z]+)`)
	// Create domain list
	domains := domainRegex.FindAllString(hostFileText, -1)
	// Return domain list
	return domains
}

// Get IPs from domain names
func getDomainIP (domains []string) []string {
	var domainIps []string
	for _, domain := range domains {
		ips, _ := net.LookupIP(domain)
		for _, ip := range ips {
			if ipv4 := ip.To4(); ipv4 != nil {
				domainIps = append(domainIps, ip.String())
			}
		}
	}
	return domainIps
}

//----------MAIN----------

func main() {
	usage := "Usage: discovery [nmap|rdns|dnsrecon|ctfr|amass|aquatone|gobuster|all] HostFile OutputDirectory [Wordlist]"

	if len(os.Args) < 3 {
		fmt.Println(usage)
		os.Exit(1)
	}

	// Parse host file
	hostFile := os.Args[2]
	hostFileContent, err := ioutil.ReadFile(hostFile)
	if err != nil {
		fmt.Println("Invalid host file")
		os.Exit(1)
	}
	hostFileText := string(hostFileContent)
	
	// Generate IP and Domain lists
	ips := createIpList(hostFileText)
	domains := createDomainList(hostFileText)
	domainIPs := getDomainIP(domains)
	// Add domain IPs to master IP list
	for _, ip := range domainIPs {
		ips = append(ips, ip)
	}
	unique.Sort(unique.StringSlice{&ips})
	unique.Strings(&ips)
	
	outDir := os.Args[3]

	wordlist := os.Args[4]

	// Parse flags
	switch os.Args[1] {
	case "nmap":
		nmapScan(ips, outDir)
	case "rdns":
		rDNS(ips, outDir)
	case "dnsrecon":
		dnsRecon(domains, outDir)
	case "ctfr":
		ctfr(domains, outDir)
	case "amass":
		amass(domains, outDir)
	case "aquatone":
		aquatone(ips, domains, outDir)
	case "gobuster":
		gobuster(domains, outDir, wordlist)
	case "all":
		nmapScan(ips, outDir)
		rDNS(ips, outDir)
		dnsRecon(domains, outDir)
		ctfr(domains, outDir)
		amass(domains, outDir)
		aquatone(ips, domains, outDir)
		gobuster(domains, outDir, wordlist)
	default:
		fmt.Println("Unknown flags")
		fmt.Println(usage)
		os.Exit(1)
	}
}