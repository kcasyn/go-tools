package main

import (
     "fmt"
     "net"
	"strings"
	"os"
	"io/ioutil"
	"regexp"
)

func main() {
     args := os.Args[1:]
     domain := args[0]

     fmt.Println("[+] Checking for MX records")
     mxs, err := net.LookupMX(domain)
     if err == nil {
     	if mxs != nil {
     		fmt.Println("[!] MX records found")
     	}

		fmt.Println("[+] Checking for SPF records")
	
     	txts, err := net.LookupTXT(domain)

		if err == nil {
			if len(txts) == 0 {
	   			fmt.Println("No SPF records found")
			}

			for _, txt := range txts {
	    			if strings.Contains(txt, "spf") {
	       			fmt.Println("[!] SPF records found")
	       			fmt.Println(txt)
	    			}
			}
		}

		fmt.Println("[+] Checking for DMARC records")
		dmarcs, err := net.LookupTXT("_dmarc." + domain)

		if err == nil {
	   		for _, dmarc := range dmarcs {
	       		fmt.Println(dmarc)
			} 
		} else {
	   		fmt.Println("No DMARC records found")
	   	}
	} else {
		fmt.Println("No MX records found")
	}

	if len(args) > 1 {
	   	headerFilePath := args[1]
	   	headerFileContent, err := ioutil.ReadFile(headerFilePath)

	   	if err != nil {
	      	panic(err)
	   	}

	   	headerText := string(headerFileContent)

	   	fmt.Println("[+] Checking for DKIM selectors")
	   	if strings.Contains(headerText, "s="){
	      	fmt.Println("[+] DKIM selectors found")
	      	regex, _ := regexp.Compile("s=[a-zA-Z0-9]*;")
	      	selectors := regex.FindAllString(headerText, -1)

	      	fmt.Println("[+] Checking DKIM records")
	      	for _, s := range selectors {
	      	  	sTrim := strings.Trim(s, "s=")
	      	  	selector := strings.Trim(sTrim, ";")
		  		fmt.Println(selector + "._domainkey." + domain)
	      	  	dkims, err := net.LookupTXT(selector + "._domainkey." + domain)

	      	  	if err == nil {
	      	     	for _, dkim := range dkims {
	      	     	 	fmt.Println(dkim)
		     		}
		  		} else {
		    	 		fmt.Println("No DKIM record found")
		  		}
	      	}
	   	}
	}
}