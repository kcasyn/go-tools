# DNS Takeover

Tool for checking for various possible domain/sub-domain/IP takeovers.

## Prerequisites

Binary needs to be run as root or have the setcap permissions set.
`setcap cap_net_raw=+ep /path/to/your/compiled/binary`

## Usage

This is the basica usage for the tool. Domains must be piped from stdin.

`cat domains.txt | dns-takeover`