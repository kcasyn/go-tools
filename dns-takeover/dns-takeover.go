package main

// package declarations
import (
	// builtins
	"bufio"
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"os"
	"strings"
	"sync"
	"time"

	// open source
	"github.com/fatih/color"
	"github.com/gammazero/workerpool"
	"github.com/go-ping/ping"
	"github.com/likexian/whois"
	whoisparser "github.com/likexian/whois-parser"
	"github.com/miekg/dns"
	"github.com/projectdiscovery/retryabledns"
)

// global variables
var (
	worker      *int
	help        *bool
	expr_day    *int
	verbose     *bool
	output      *string
	mutex       sync.Mutex
	urls        []string
	record_info = make(map[string][]string)
)

// initialize variables
func init() {
	expr_day = flag.Int("expire-day", 30, "Estimated days for expiration")
	verbose = flag.Bool("v", false, "Print all logs")
	worker = flag.Int("w", 8, "number of workers")
	output = flag.String("output", "", "Save output to file as json")
	help = flag.Bool("h", false, "help")
}

func main() {
	// parse cli flags
	flag.Parse()

	// show help
	if *help {
		flag.PrintDefaults()
		os.Exit(1)
	}

	// print variable config
	if *verbose {
		printConf()
	}

	// read stdin input
	fi, _ := os.Stdin.Stat()
	// check for data
	if fi.Mode()&os.ModeNamedPipe == 0 {
		color.Red("No data found in pipe. urls must given using pipe!")
		os.Exit(1)
	} else {
		readFromStdin()
	}

	// print start time
	color.Cyan("[*] Scan Starting Time: %s", time.Now().Format("2006-01-02 15:04:05"))

	// print numberr of domains being checked
	len_url := len(urls)
	color.Cyan("[*] %d domains will be scanned.", len_url)

	// create worker pool
	wp := workerpool.New(*worker)

	// make DNS requests using workers
	for _, r := range urls {
		r := r
		wp.Submit(func() {
			getDNSRecord(r)
		})
	}

	// stop worker pool, waiting for all workers to complete
	wp.StopWait()
	// print end time after all workers complete
	defer color.Cyan("[*] End Time: %s", time.Now().Format("2006-01-02 15:04:05"))

	// perform whois lookups as workers complete
	defer whoisDomain(record_info)

	// write data to file
	if *output != "" {
		defer writeToFile(*output, record_info)
	}
}

// read piped data from stdin
func readFromStdin() {
	// create buffer and read data
	scanner := bufio.NewScanner(os.Stdin)
	// split data into lines
	scanner.Split(bufio.ScanLines)

	// for each line in buffer
	for scanner.Scan() {
		// load text
		u := scanner.Text()
		// remove http/https strings from domains
		if strings.HasPrefix(u, "https://") {
			url := strings.ReplaceAll(u, "https://", "")
			urls = append(urls, url)

		} else if strings.HasPrefix(u, "http://") {
			url := strings.ReplaceAll(u, "http://", "")
			urls = append(urls, url)
		} else {
			urls = append(urls, u)
		}
	}
}

// make DNS requests
func getDNSRecord(domain string) {
	// set DNS resolvers
	resolvers := []string{"1.1.1.1:53", "8.8.8.8:53", "8.8.4.4:53", "1.0.0.1:53"}
	// set max retries to 2
	retries := 2
	// set domain
	hostname := domain
	// slice to hold DNS records
	var records []string
	// slice for the record types we want
	recordTypes := []string{"CNAME", "NS", "MX", "SPF", "A"}
	// create a DNS client
	dnsClient := retryabledns.New(resolvers, retries)
	// resolve the domain
	_, err := dnsClient.Resolve(hostname)
	if err != nil {
		color.Yellow("%s -> %s skipping...", err, hostname)
	}

	// perform request based on the record type we want
	for _, rType := range recordTypes {
		switch rType {
		case "CNAME":
			dnsResponses, err := dnsClient.Query(hostname, dns.TypeCNAME)
			if err != nil {
				color.Yellow("%s -> %s skipping %s...", err, hostname, rType)
			}

			if dnsResponses.CNAME != nil {
				for _, r := range dnsResponses.CNAME {
					records = append(records, r)
				}
			}

			if *verbose {
				color.White("%s %s %s", domain, rType, dnsResponses.CNAME)
			}
		case "NS":
			dnsResponses, err := dnsClient.Query(hostname, dns.TypeNS)
			if err != nil {
				color.Yellow("%s -> %s skipping %s...", err, hostname, rType)
			}

			if dnsResponses.NS != nil {
				for _, r := range dnsResponses.NS {
					records = append(records, r)
				}
			}

			if *verbose {
				color.White("%s %s %s", domain, rType, dnsResponses.NS)
			}
		case "MX":
			dnsResponses, err := dnsClient.Query(hostname, dns.TypeMX)
			if err != nil {
				color.Yellow("%s -> %s skipping %s...", err, hostname, rType)
			}

			if dnsResponses.MX != nil {
				for _, r := range dnsResponses.MX {
					records = append(records, r)
				}
			}

			if *verbose {
				color.White("%s %s %s", domain, rType, dnsResponses.MX)
			}
		case "SPF":
			dnsResponses, err := dnsClient.Query(hostname, dns.TypeTXT)
			if err != nil {
				color.Yellow("%s -> %s skipping %s...", err, hostname, rType)
			}

			if dnsResponses.TXT != nil {
				for _, r := range dnsResponses.TXT {
					if strings.Contains(r, "spf") {
						color.Magenta("%s SPF %s", domain, r)
					}
				}
			}

			if *verbose {
				color.White("%s TXT %s", domain, dnsResponses.TXT)
			}
		case "A":
			dnsResponses, err := dnsClient.Query(hostname, dns.TypeA)
			if err != nil {
				color.Yellow("%s -> %s skipping %s...", err, hostname, rType)
			}

			if dnsResponses.A != nil {
				for _, r := range dnsResponses.A {
					pinger, err := ping.NewPinger(r)
					if err != nil {
						color.Yellow("%s -> %s skipping %s...", err, hostname, rType)
					}

					pinger.SetPrivileged(true)
					pinger.Count = 1

					err = pinger.Run() // Blocks until finished.
					if err != nil {
						color.Red("Error Detected! Domain: %s Error: %s", domain, err)
					}

					stats := pinger.Statistics()

					if stats.PacketsRecv == 0 {
						color.Green("[+] Dangling IP was detected! %s IN %s", domain, r)
					}
				}
			}
		default:
			color.White("Record type: %s", rType)
		}
	}

	// map the returned DNS records to the domain
	if records != nil {
		record_info[domain] = records
	}
}

// perform whois lookups
func whoisDomain(domains map[string][]string) {
	// perform a whois for each domain in a DNS record
	for domain, dmn := range domains {
		resp_whois, err := whois.Whois(domain)
		if err == nil {
			if result, err := whoisparser.Parse(resp_whois); err == nil {
				// check the domain expiration date
				if result.Domain.ExpirationDate != "" {
					// print the expiration date
					expireDomain(domain, result.Domain.ExpirationDate, dmn)
				}
				// check for unregistered domains
			} else if err.Error() == "whoisparser: domain is not found" {
				color.Green("[+] Unregistered domain was detected! %s IN %s", domain, dmn)
			} else {
				color.Red("Error Detected! Domain: %s Error: %s", domain, err)
			}
		}
	}
}

// print the domain expiration date
func expireDomain(domain, expire_date string, dmn []string) {
	date := time.Now()
	format := "2006-01-02T15:04:05Z"
	then, _ := time.Parse(format, expire_date)
	diff := then.Sub(date)
	days_remain := int(diff.Hours() / 24)
	len_dmn := len(dmn)
	if days_remain < *expr_day {
		color.Magenta("[+] %s will be expired after [%d] days. It being used by %d diffirent domains. Expire Time: [%s]. Domains that are used by %s:", domain, days_remain, len_dmn, expire_date, domain)
		fmt.Println(dmn)
	}
}

// write results to a file
func writeToFile(filename string, data map[string][]string) error {
	file, err := os.Create(filename)
	if err != nil {
		return err
	}
	defer file.Close()
	map_to_json, err := json.Marshal(data)
	if err != nil {
		fmt.Println("Error: ", err.Error())
	} else {
		_, err = io.WriteString(file, string(map_to_json))
		if err != nil {
			return err
		}
	}
	color.Cyan("[*] Scan results was saved to %s", filename)
	return file.Sync()
}

// print the configuration
func printConf() {
	fmt.Printf(`
_____________________________________________
Worker      	: %d
Max Expire Day	: %d
Verbose      	: %t
Output File  	: %s
_____________________________________________
`, *worker, *expr_day, *verbose, *output)
}
